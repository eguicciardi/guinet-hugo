---
title: "Here Today"
date: 2021-03-02T17:52:41+01:00
draft: true
---
And if I say
I really knew you well
What would your answer be?
If you were here today
