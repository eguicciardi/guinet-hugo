---
title: "Asimov Right"
date: 2021-03-09T15:08:10+01:00
author: "Isaac Asimov"
source: "Foundation"
authorurl: ""
sourceurl: ""
draft: true
---
Never let your sense of morals prevent you from doing what is right.
