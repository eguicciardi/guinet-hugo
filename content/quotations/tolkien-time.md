---
title: "Tolkien Time"
date: 2021-03-09T15:11:11+01:00
author: "J.R.R. Tolkien"
source: "The Fellowship of the Ring"
authorurl: ""
sourceurl: ""
draft: true
---
All we have to decide is what to do with the time that is given us.