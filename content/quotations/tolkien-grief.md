---
title: "Tolkien Grief"
date: 2021-03-09T15:12:33+01:00
author: "J.R.R. Tolkien"
source: "The Return of the King"
authorurl: ""
sourceurl: ""
draft: true
---
I will not say: do not weep; for not all tears are an evil.